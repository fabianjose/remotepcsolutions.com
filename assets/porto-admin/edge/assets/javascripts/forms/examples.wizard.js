
/*
Name: 			Forms / Wizard - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.2
*/

(function( $ ) {

	'use strict';

	
	/*
	Wizard #4
	*/
	var $w4finish = $('#w4').find('ul.pager li.finish'),
		$w4validator = $("#w4 form").validate({
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
			$(element).remove();
		},
		errorPlacement: function( error, element ) {
			element.parent().append( error );
		}
	});

	$w4finish.on('click', function( ev ) {
		ev.preventDefault();
		var validated = $('#w4 form').valid();
		if ( validated ) {
			new PNotify({
				title: 'Felicitaciones',
				text: 'Has Completado todos los pasos.',
				type: 'custom',
				addclass: 'notification-success',
				icon: 'fa fa-check'
			});
			$('.previous').hide();
		}
	});

	
mover();
var index=1;	
	index++;



 $(".readonly").keydown(function(e){
        e.preventDefault();
    });

function mover(){

	$('#w4').bootstrapWizard({
		tabClass: 'wizard-steps',
		nextSelector: 'ul.pager li.next',
		previousSelector: 'ul.pager li.previous',
		firstSelector: null,
		lastSelector: null,
		onNext: function( tab, navigation, index, newindex ) {
			var validated = $('#w4 form').valid();
			if( !validated ) {
				$w4validator.focusInvalid();
				return false;
			}
		},
		onTabClick: function( tab, navigation, index, newindex ) {
			if ( newindex == index + 1 ) {
				//console.log("indices: " + index);

				return this.onNext( tab, navigation, index, newindex);
			} else if ( newindex > index + 1 ) {
				return false;
			} else {
				return true;
			}
		},
		onTabChange: function( tab, navigation, index, newindex ) {
			var $total = navigation.find('li').size() - 1;
			//console.log("indices: " + index);
			$w4finish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
			$('#w4').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
		},
		onTabShow: function( tab, navigation, index ) {
			var $total = navigation.find('li').length - 1;
			var $current = index;
			var $percent = Math.floor(( $current / $total ) * 100);
			$('#w4').find('.progress-indicator').css({ 'width': $percent + '%' });
			tab.prevAll().addClass('completed');
			tab.nextAll().removeClass('completed');
			if(index==1){
				var zona=$('#w4-pais').val();
				localStorage.setItem("zona", zona);
				console.log("pais seleccionado: "+$('#w4-pais').val());
							var fecha = "";
							var evento = function(msg){
									console.log(msg);
									var horas=msg;

							var fechaSeleccionada=new Date(), horaseleccionada="";
							console.log("fecha seleccionado: "+fechaSeleccionada);
							var  month2 = '' + (fechaSeleccionada.getMonth() + 1),
											        day2= '' + fechaSeleccionada.getDate(),
											        year2 = fechaSeleccionada.getFullYear(),
											        hora2= fechaSeleccionada.getHours(),
											        min2= fechaSeleccionada.getMinutes();	
							fechaSeleccionada=year2+"/"+month2+"/"+day2;
							var validarfecha3="";
							var actualmente=new Date();
							var  mesactual= '' + (actualmente.getMonth() + 1),
											        diaactual= '' + actualmente.getDate(),
											        añoactual = actualmente.getFullYear();	

							jQuery.datetimepicker.setLocale('es');
							jQuery('#datetimepicker').datetimepicker({
							  //format:'Y.m.d',	
							 minDate:'-0',
							 maxDate:'+1970/12/31',
							  i18n:{
							  es:{
							   months:[
							    'Enero','Febrero','Marzo','Abril',
							    'Mayo','Junio','Julio','Agosto',
							    'Septiembre','Octubre','Noviembre','Diciembre',
							   ],
							   dayOfWeek:[
							    "Lu", "Ma", "Mi", "Jue", 
							    "Do", "Vie", "Sa",
							   ]
							     
							  }
							 },
							  inline:true,	
							  scrollTime:true,
							  validateOnBlur:true,	 
							  allowTimes:[
								  '00:00', '2:00', '4:00', '6:00', '8:00', '10:00', 
								  '12:00', '14:00', '16:00', '18:00', '20:00',  '22:00'
								 ],
							 onSelectDate: function(dp, $input) {
						 			var  month = '' + (dp.getMonth() + 1),      day= '' + dp.getDate(),     year = dp.getFullYear();	
								 	if(diaactual==day&&mesactual==month&&añoactual==year){
						 				$('#w4-fecha').val("");
						 				mensaje.innerHTML="";
								 	}
								 	else{			 				
											 var  month2 = '' + (dp.getMonth() + 1), day2= '' + dp.getDate(),    year2 = dp.getFullYear(),
											hora2= dp.getHours(),   min2= dp.getMinutes();	
											fechaSeleccionada=year2+"/"+month2+"/"+day2;
											validarfecha3=fechaSeleccionada+" "+horaseleccionada;
											if (fechaSeleccionada!=""&&horaseleccionada!="")
												validarfecha(validarfecha3);
											else{
													$('#w4-fecha').val("");
											}
								 	}
							  },
							  onSelectTime: function (current_time,$input){
								  	var fecha=current_time;
						  			var  month = '' + (fecha.getMonth() + 1),   day= '' + fecha.getDate(),      year = fecha.getFullYear();	
								 	if(diaactual==day&&mesactual==month&&añoactual==year){
								 		console.log("hora actual seleccionado");
								 	}				  
								  	var hora= fecha.getHours(),  min= fecha.getMinutes();	
								    horaseleccionada=hora+":"+min+"0";
								    var  month2 = '' + (fecha.getMonth() + 1),  day2= '' + fecha.getDate(),
													        year2 = fecha.getFullYear(),     hora2= fecha.getHours(),  min2= fecha.getMinutes();	
									fechaSeleccionada=year2+"/"+month2+"/"+day2;


								    validarfecha3=fechaSeleccionada+" "+horaseleccionada;
								    if (fechaSeleccionada!=""&&horaseleccionada!="")
							 			validarfecha(validarfecha3);
							 		else{
							 				$('#w4-fecha').val("");
							 		}

							  }
							 
							  
							});
							
									
							
				
					}	

			var data={id:10, zona:$('#w4-pais').val()};
			ajax("modelo/citas.php", data, false, false, "Obtener horas examples.wizard.js",  evento);

			var fecha="";
			 $(".readonly").keydown(function(e){
			        e.preventDefault();
		    });

			}
			if(index==2){
				$('#procesar_pago .next').hide();
				/*localStorage.setItem("nombre", );
				localStorage.setItem("correo", );
				localStorage.setItem("fecha", );
				localStorage.setItem("descripcion", );*/
				var nombre, correo, fecha, descripcion,zona;
				nombre= $('#w4-nombre').val();
				correo=$('#w4-correo').val();
				fecha=$('#w4-fecha').val();
				descripcion=$('#w4-descripcion').val();
				zona=$('#w4-pais').val();
				localStorage.setItem("zona", zona);
				var fecha2=fecha;
				var res=fecha2.replace(/\//g, "-");

				var evento = function(msg){
					console.log(msg);
				}
				var data={id:11, nombre:nombre, email:correo, fecha:res, descripcion:descripcion, zona:zona};
			    ajax("modelo/citas.php", data, false, false, "guardar cliente",  evento);	
				


			}else{
				$('#procesar_pago .next').show();
			}
			
		}
	});
	


}

var $w6finish = $('#w6').find('ul.pager li.finish'),
		$w6validator = $("#w6 form").validate({
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
			$(element).remove();
		},
		errorPlacement: function( error, element ) {
			element.parent().append( error );
		}
	});


var $w6finish = $('#w6').find('ul.pager li.finish'),
		$w6validator = $("#w6 form").validate({
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
			$(element).remove();
		},
		errorPlacement: function( error, element ) {
			element.parent().append( error );
		}
	});

	$w6finish.on('click', function( ev ) {
		ev.preventDefault();
		var validated = $('#w6 form').valid();
		if ( validated ) {
			new PNotify({
				title: 'Felicitaciones',
				text: 'Has completado todos los pasos.',
				type: 'custom',
				addclass: 'notification-success',
				icon: 'fa fa-check'
			});
			$('.previous').hide();
		}
	});


$('#w6').bootstrapWizard({
		tabClass: 'wizard-steps',
		nextSelector: 'ul.pager li.next',
		previousSelector: 'ul.pager li.previous',
		firstSelector: null,
		lastSelector: null,
		onNext: function( tab, navigation, index, newindex ) {
			var validated = $('#w6 form').valid();
			if( !validated ) {
				$w6validator.focusInvalid();
				return false;
			}
		},
		onTabClick: function( tab, navigation, index, newindex ) {
			if ( newindex == index + 1 ) {
				//console.log("indices: " + index);

				return this.onNext( tab, navigation, index, newindex);
			} else if ( newindex > index + 1 ) {
				return false;
			} else {
				return true;
			}
		},
		onTabChange: function( tab, navigation, index, newindex ) {
			var $total = navigation.find('li').size() - 1;
			//console.log("indices: " + index);
			$w6finish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
			$('#w6').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
		},
		onTabShow: function( tab, navigation, index ) {
			var $total = navigation.find('li').length - 1;
			var $current = index;
			var $percent = Math.floor(( $current / $total ) * 100);
			$('#w6').find('.progress-indicator').css({ 'width': $percent + '%' });
			tab.prevAll().addClass('completed');
			tab.nextAll().removeClass('completed');
			//console.log("indice: "+index);
			if(index==1){
				$('#procesar_pago2 .next').hide();
				localStorage.setItem("nombre2", $('#w6-nombre').val());
				localStorage.setItem("correo2", $('#w6-correo').val());
				localStorage.setItem("descripcion2", $('#w6-descripcion').val());

				var nombre, correo, descripcion;
				nombre= $('#w6-nombre').val();
				correo=$('#w6-correo').val();
				//fecha=$('#w4-fecha').val();
				descripcion=$('#w6-descripcion').val();

				/*var fecha2=fecha;
				var res=fecha2.replace(/\//g, "-");*/

				var evento = function(msg){
					console.log(msg);
				}
				var data={id:12, nombre:nombre, email:correo, descripcion:descripcion};
			    ajax("modelo/citas.php", data, false, false, "guardar cliente",  evento);	
				

			}else{
				$('#procesar_pago2 .next').show();
			}
			
		}
	});
	

}).apply( this, [ jQuery ]);
